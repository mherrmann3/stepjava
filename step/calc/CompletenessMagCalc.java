package org.opensha.step.calc;

import org.apache.log4j.Logger;
import org.opensha.sha.earthquake.observedEarthquake.ObsEqkRupList;
import org.opensha.sha.earthquake.observedEarthquake.ObsEqkRupture;
import org.opensha.sha.magdist.GutenbergRichterMagFreqDist;

import java.util.ListIterator;



/**
 * <p>Title: CompletenessMagCalc</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class CompletenessMagCalc {
	private static Logger logger = Logger.getLogger(CompletenessMagCalc.class);
	
//TODO suggestions: change variables to non static, and let methods to return the result
  public  double mcBest;
  public  double mcSynth;
  public  double mcMaxCurv;
  public  int numInBin;
  private  double deltaBin = 0.1;
  private  double GR_MMax = 10.0; // max mag to use in GR calc

  public CompletenessMagCalc() {

  }

  public CompletenessMagCalc(ObsEqkRupList afterShocks) {
	  setMcBest(afterShocks);
}

/**
   * set_McBest
   * Calculate the best Mc estimate based on the synthetic and max curvature
   * methods
   */
  public  void setMcBest(ObsEqkRupList eventList) {
    ListIterator eventIt = eventList.listIterator();
    ObsEqkRupture event;
    int numEvents = eventList.size();
    int ind = 0;
    double[] magList = new double[numEvents];
    while (eventIt.hasNext())  {
      event = (ObsEqkRupture)eventIt.next();
      magList[ind++] = event.getMag();
    }
    calcMcSynth(magList);
  }

  /**
   * setMcMaxCurv
   */
  public void setMcMaxCurv(ObsEqkRupList eventList) {
    ListIterator eventIt = eventList.listIterator();
    ObsEqkRupture event;
    int numEvents = eventList.size();
    int ind = 0;
    double[] magList = new double[numEvents];
    while (eventIt.hasNext())  {
      event = (ObsEqkRupture)eventIt.next();
      magList[ind++] = event.getMag();
    }
    calcMcMaxCurv(magList);
  }

  /**
   *
   * @return mcBest double
   */
  public  double getMcBest(){
	  // not reasonable check, since getMcSynth calculates both mcMaxCurv and mcSynth
	  //  (and uses the first, if it can not obtain the latter)
//    if (mcSynth != -99)
//        mcBest = mcSynth;
//      else
//        mcBest = mcMaxCurv;
//    return mcBest;
    return mcSynth;
    }

    /**
     * Calculate Mc based on the max curvature method
     * @return mcMaxCurv double
     */
    public  double getMcMaxCurv(){
      return mcMaxCurv;
    }

    /**
     * Calculate Mc based on synthetic GR distributions.  Return the value
     * estimated at 95% probability, if not return the 90%.  If this is not
     * possible to estimate, return a -99
     * @return double
     */
    public  double getMcSynth(){

      //HOW TO BETTER HANDLE THE -99?!?!?!?!?!
      return mcSynth;
    }

    private  void calcMcMaxCurv(double[] magList){

      double minMag = ListSortingTools.getMinVal(magList);
      double maxMag = ListSortingTools.getMaxVal(magList);
//      if(minMag>0){
//        minMag=0;
//      }

      //number of mag bins
      int numMags = (int)(maxMag*10)+1;

      //create the histogram of the mag bins
      MagHist hist = new MagHist(magList,minMag-(deltaBin/2),maxMag+(deltaBin/2),deltaBin);
      int[] numInBin = hist.getNumInBins();
      // find the value of max curvature and the bin it corresponds to
      double maxCurv = ListSortingTools.getMaxVal(numInBin);
      double[] magRange = ListSortingTools.getEvenlyDiscrVals(minMag,maxMag,deltaBin);
      int maxCurvInd = ListSortingTools.findIndex((int)maxCurv,numInBin);
      //set mc to the M value at the maximum curvature
      mcMaxCurv = magRange[maxCurvInd];
    }
    
    /**
     * Calculate Mc based on synthetic GR distributions
     * (Goodness-of-Fit [Wiemer and Wyss, 2000]
     */
    private  void calcMcSynth(double[] magList){
      // [MH] if you want max curvature MC, then calculate it first, dude!
   	  calcMcMaxCurv(magList);
      // make a first guess at Mc using max curvature
      mcMaxCurv = getMcMaxCurv();
         
      int aSize = (int)((((mcMaxCurv+1.5)-(mcMaxCurv-0.9))/deltaBin)+1.0);
      double[] fitProb = new double[aSize];
      int ct = 0;
      // loop over a range of completeness guesses
      //double topVal = mcMaxCurv + 1.5;      
      for(double mcLoop = mcMaxCurv-0.9; mcLoop < mcMaxCurv + 1.5;
          mcLoop += deltaBin){
        //double[] magBins = new double[numEvents];
      // get all events above the completeness guess (mcLoop)
        double[] cutCat = null;
        int sizeCutCat = 0;
        try{//TODO check, the could be no value found in the magList for the provided top value mcMaxCurv + 1.5
        	// in this case, simply ignore the error, correct??
        	// [MH] Included the "half bin" below the mcloop
        	cutCat = ListSortingTools.getValsAbove(magList,mcLoop-(deltaBin/2));
            //
            sizeCutCat = cutCat.length;
          }
          catch (NoValsFoundException err1){
        	  if (mcLoop <= mcMaxCurv) { // Only give error msg when we really have to worry about
        	  	logger.error("-- calcMcSynth error (not enough earthquakes) " + err1 );
//        	  logger.info("    mcLoop: " + mcLoop + " magListLen: " + magList.length + " mcMaxCurv: " + mcMaxCurv);
        	  }
        	  //continue;
          }

        // if > 25 events calculate the b value and estimate Mc
        if(sizeCutCat >= 25){
          MaxLikeGR_Calc maxLikeGR_Calc = new MaxLikeGR_Calc();
          maxLikeGR_Calc.setMags(cutCat);         
          double bvalMaxLike = maxLikeGR_Calc.get_bValueMaxLike();
          int numBins = (int)Math.round((GR_MMax-mcLoop)/deltaBin)+1;

          // create the GR distribution of synthetic events
          GutenbergRichterMagFreqDist GR_FMD =
              new GutenbergRichterMagFreqDist(mcLoop,GR_MMax,numBins);
          GR_FMD.setAllButTotMoRate(mcLoop,GR_MMax,sizeCutCat,bvalMaxLike);
          // loop over all bins and get the # of synthetic  events in each bin
          int mIndex = 0;
          double[] mbinRates = new double[numBins];
          for(double mbinLoop = mcLoop; mbinLoop <= GR_MMax; mbinLoop += deltaBin){
//            mbinRates[mIndex] = GR_FMD.getIncrRate(mIndex++);
        	  //[MH] Actually, it should be the CUMULATIVE rate...
            mbinRates[mIndex] = Math.round(GR_FMD.getCumRate(mIndex++));
            
          }
//          double sumSynth = ListSortingTools.getListSum(mbinRates);

          //create the histogram of the observed events (in mag bins)
          boolean flip = true;
//          MagHist hist = new MagHist(magList,mcLoop,GR_MMax,deltaBin);
          // [MH] Mind the Hist-definition: it needs the EDGES, not the CENTERS...
          MagHist hist = new MagHist(magList,mcLoop-(deltaBin/2),GR_MMax+(deltaBin/2),deltaBin);
          int[] numObsInBin = hist.getNumInBins();
          double[] obsCumSum = ListSortingTools.calcCumSum(numObsInBin,flip);
          double sumObs = ListSortingTools.getListSum(obsCumSum);
          double numer = 0;

          // calculate the fit of the synthetic to the real
          for(int sLoop = 0; sLoop < obsCumSum.length; ++ sLoop){
            numer = numer + Math.abs(obsCumSum[sLoop]-mbinRates[sLoop]);
          }
//          fitProb[ct] = (numer/sumObs)*100.0;
          // [MH] In fact, the "*FIT* probability" is 1-X
          fitProb[ct] = 100-(numer/sumObs)*100.0;
        }
       ++ct;  // increment the mcLoop counter
      }
      try {
    	  // First, try to get fMC95 ...
          double[] mc95List = ListSortingTools.getValsAbove(fitProb,95.0);
//          mcSynth = ListSortingTools.getMinVal(mc95List); //????? change to class level variable
          // [+MH] correction: Get the first magnitude cutoff that results in > 0.95 fit 
	      int mcSynthInd = ListSortingTools.findIndex(mc95List[0],fitProb);
	      mcSynth = mcMaxCurv-0.9 + deltaBin*mcSynthInd;
      } catch (NoValsFoundException err1){
    	  try {
    		  // ... otherwise, try to get fMC90
    		  double[] mc90List = ListSortingTools.getValsAbove(fitProb, 90.0);
//    		  mcSynth = ListSortingTools.getMinVal(mc90List); //???change to class level variable
    		  // [+MH] correction again
    		  int mcSynthInd = ListSortingTools.findIndex(mc90List[0],fitProb);
    		  mcSynth = mcMaxCurv-0.9 + deltaBin*mcSynthInd;
//           } catch (NoValsFoundException err2){
//        	   try {
//            	   // ... otherwise, get the best fit MC
//        		   double mcUnknList = ListSortingTools.getMaxVal(fitProb);
//        		   int mcSynthInd = ListSortingTools.findIndex(mcUnknList,fitProb);
//        		   mcSynth = mcMaxCurv-0.9 + deltaBin*mcSynthInd;
        	   } catch (NoValsFoundException err3) {
        		   // ..otherwise return M of maximum curvature
        		   mcSynth = mcMaxCurv;
//        		   mcSynth = Double.NaN; //????change to class level variable
		           //err2.printStackTrace();
//        	   }
           }
      }
//      logger.info("mcSynth:" + mcSynth);
    }


}
