package org.opensha.step.calc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

import org.opensha.commons.data.Location;
import org.opensha.commons.data.LocationList;
import org.opensha.commons.data.region.GriddedRegion;
import org.opensha.sha.earthquake.griddedForecast.STEP_CombineForecastModels;
import org.opensha.sha.earthquake.observedEarthquake.ObsEqkRupList;
import org.opensha.sha.earthquake.observedEarthquake.ObsEqkRupture;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2002</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class STEP_TypeIIAftershockZone_Calc {
  //private ObsEqkRupList newObsEventsList;
  private GriddedRegion typeIIAftershockZone;
  private LocationList faultSegments;
  private double zoneRadius, gridSpacing;
  private Comparator <Location> locationComparatorByLong = new Comparator <Location> (){
		@Override
		public int compare(Location loc1, Location loc2) {
			int longdiff = (int)(100*loc1.getLongitude() - 100*loc2.getLongitude());
			if(longdiff == 0){
				return (int)(100*loc1.getLatitude() - 100*loc2.getLatitude());
			}
			return longdiff;
		}    	
  }; 
  
  private Comparator <Location> locationComparatorByLat = new Comparator <Location> (){
		@Override
		public int compare(Location loc1, Location loc2) {
			int latdiff = (int)(100*loc1.getLatitude() - 100*loc2.getLatitude());
			if(latdiff == 0){
				return (int)(100*loc1.getLongitude() - 100*loc2.getLongitude());
			}
			return latdiff;
		}    	
  };
  

  public STEP_TypeIIAftershockZone_Calc(ObsEqkRupList newObsEventsList, STEP_CombineForecastModels aftershockModel) {
    ObsEqkRupture mainshock = aftershockModel.getMainShock();
    Location mainshockLoc = mainshock.getHypocenterLocation();
    //TODO double check the following 2 variables should be class level
     gridSpacing = aftershockModel.get_GridSpacing();
     zoneRadius = aftershockModel.get_AftershockZoneRadius();
    //double faultRadius = aftershockModel.get_AftershockZoneRadius();
    calc_SyntheticFault(newObsEventsList, mainshockLoc);
  }

  /**
   * calc_SyntheticFault
   * This method is not yet finished.  A sort method is needed in LocationList
   * to sort on lats and longs.
   */
  @SuppressWarnings("null")
public  void calc_SyntheticFault(ObsEqkRupList newObsEventsList, Location mainshockLoc) {
    ListIterator eventIt = newObsEventsList.listIterator();
    int numEvents = newObsEventsList.size();
    //double[] eLat = new double[numEvents];
    //double[] eLong = new double[numEvents];
    ObsEqkRupture event = new ObsEqkRupture();
    Location eLoc = new Location();
    //LocationList latLongList = new LocationList();
    List <Location> latLongList = new ArrayList <Location> ();

    while (eventIt.hasNext()){
      event = (ObsEqkRupture)eventIt.next();
      eLoc = event.getHypocenterLocation();
      latLongList.add (eLoc);
    }
    
    int newFaultApprox = 1;
    
    Location topEndPoint = new Location();
    Location bottomEndPoint = new Location();    
    if (newFaultApprox == 0) {
	    /**
	     * sort the lat long pairs and ignore the extreme values (.01 and .99)
	     */
	    int minInd = (int)Math.round(0.01*numEvents);
	    int maxInd = (int)Math.round(0.99*numEvents);
	    int numIn = (int)Math.round(.8*numEvents);
	   
	    //Arrays.sort(eLat);
	    //Arrays.sort(eLong);  
	    //1. sort locations by lattitude 
	    Collections.sort(latLongList,locationComparatorByLat);
	    
	    double maxLat_LatSort =  latLongList.get(maxInd).getLatitude();//eLat[maxInd];
	    double minLat_LatSort =  latLongList.get(minInd).getLatitude(); //eLat[minInd];
	    double maxLong_LatSort = latLongList.get(maxInd).getLongitude();
	    double minLong_LatSort = latLongList.get(minInd).getLongitude();
	    
	    //2. sort by longitude     
	    Collections.sort(latLongList,locationComparatorByLong);
	    
	    double maxLong_LongSort = latLongList.get(maxInd).getLongitude();//eLong[maxInd];
	    double minLong_LongSort = latLongList.get(minInd).getLongitude();//eLong[minInd];
	    double maxLat_LongSort = latLongList.get(maxInd).getLatitude();//0;
	    double minLat_LongSort = latLongList.get(minInd).getLatitude();;
	
	    /**
	     * THESE WILL NEED TO BE SET ONCE THE SORT METHOD IS
	     * implemented in LocationList
	     */ 
	    double latDiff = maxLat_LatSort-minLat_LatSort;
	    double longDiff = maxLong_LongSort-minLong_LongSort;
	
	    /** find the largest dimension - either in Lat or in Long
	     *  this needs to be improved
	     */
	
	    double topEventLat, topEventLong, bottomEventLat, bottomEventLong;
	    if (latDiff > longDiff){
	      topEndPoint.setLatitude(maxLat_LatSort);
	      topEndPoint.setLongitude(maxLong_LatSort);
	      bottomEndPoint.setLatitude(minLat_LatSort);
	      bottomEndPoint.setLongitude(minLong_LatSort);
	    }
	    else {
	      topEndPoint.setLatitude(maxLat_LongSort);
	      topEndPoint.setLongitude(maxLong_LongSort);
	      bottomEndPoint.setLatitude(minLat_LongSort);
	      bottomEndPoint.setLongitude(minLong_LongSort);
	    }
    }
    else {

	    /**
	     * [MH] new fault approximation
	     */
	    
	    float[] eLat = new float [ latLongList.size() ];
	    float[] eLon = new float [ latLongList.size() ];
	    for( int i = 0; i < latLongList.size(); i++ ) {
	    	eLat[i] = (float)latLongList.get(i).getLatitude();
	    	eLon[i] = (float)latLongList.get(i).getLongitude();
	    }
	    
	    float minLat = (float) Math.round(RegionDefaults.searchLatMin*10)/10;
	    float maxLat = (float)RegionDefaults.searchLatMax;
	    float minLon = (float)RegionDefaults.searchLongMin;
	    float maxLon = (float)RegionDefaults.searchLongMax;
	    //refine sizes of boxes to get better results
	    float boxSize = (float)(0.5*RegionDefaults.gridSpacing);
	    // create a 2D Histogram
	    short[] eventMap = eventMapFromCatalog(eLat, eLon, minLat, maxLat, minLon, maxLon, boxSize);
	    
	    // create a coordinate list corresponding to eventMap
	    float[] eventMapLats = new float [ eventMap.length];
	    float[] eventMapLons = new float [ eventMap.length];
	    int numberOfLonBoxes =  Math.round((maxLon - minLon) / boxSize);
		for( int i = 0; i < Math.round((maxLat - minLat) / boxSize); i++ ) {
			for( int j = 0; j < numberOfLonBoxes; j++ ) {
				eventMapLats[i*numberOfLonBoxes+j] = minLat + i*boxSize + (float)(0.5*boxSize);
				eventMapLons[i*numberOfLonBoxes+j] = minLon + j*boxSize + (float)(0.5*boxSize);
			}
		}
	    
	    // get the mean count of bins > 0
	    short sumCounts = 0;
	    short sumCountsLength = 0;
	    for( int i = 0; i < eventMap.length; i++ ) {
	    	if (eventMap[i] > 0) {
	    		sumCounts += eventMap[i];
	    		sumCountsLength++;
	    	}
	    }
	    // criteria for truncating the 2D Histogram
	    float levelTrunc = 2 * ((float)sumCounts/(float)sumCountsLength);
	    
	    // reduce the histogram to the remaining bins representing the fault
	    // 1. how many bins are left (not elegant)
	    int numFaultBins = 0; 
	    for( int i = 0; i < eventMap.length; i++ ) {
	    	if (eventMap[i] > levelTrunc)
	    		numFaultBins++;
	    }
	    // 2. get the remaining bins + their locations
	    int k = 0;
	    float[] faultLats = new float [ numFaultBins ];
	    float[] faultLons = new float [ numFaultBins ];
	    for( int i = 0; i < eventMap.length; i++ ) {
	    	if (eventMap[i] > levelTrunc) {
	    		faultLats[k] = eventMapLats[i];
	    		faultLons[k] = eventMapLons[i];
	    		k++;
	    	}
	    }
	    
	    // get the 2 most distant bins
//	    double[][] distMat = new double [numFaultBins][numFaultBins];
	    double dist = 0;
	    double distMax = 0;
	    double endPoint1Lat = 0; double endPoint1Lon = 0;
	    double endPoint2Lat = 0; double endPoint2Lon = 0;
		for( int i = 0; i < numFaultBins; i++ ) {
			for( int j = i; j < numFaultBins; j++ ) {
//				distMat[i][j] = vincentyDistanceBetweenPoints(faultLats[i],faultLons[i],faultLats[j],faultLons[j]);
				dist = vincentyDistanceBetweenPoints(faultLats[i],faultLons[i],faultLats[j],faultLons[j]);
				if (dist > distMax) {
					distMax = dist;
					// get coordinates
					endPoint1Lat = faultLats[i]; endPoint1Lon = faultLons[i];
					endPoint2Lat = faultLats[j]; endPoint2Lon = faultLons[j];
				}
			}
		}
		
		//assign the coordinates the the EndPoints
		topEndPoint.setLatitude(endPoint1Lat);
		topEndPoint.setLongitude(endPoint1Lon);
		bottomEndPoint.setLatitude(endPoint2Lat);
		bottomEndPoint.setLongitude(endPoint2Lon);
		
    }
    
    /**
     * Create a two segment fault that passes thru the mainshock
     * using the extreme widths defined above
     */
    faultSegments = new LocationList();
    faultSegments.addLocation(topEndPoint);
    faultSegments.addLocation(mainshockLoc);
    //faultSegments.addLocation(mainshockLoc);//TODO check correct?
    faultSegments.addLocation(bottomEndPoint);
  }

  /**
   * CreateAftershockZoneDef
   */
  public void CreateAftershockZoneDef() {
	  typeIIAftershockZone =
        new GriddedRegion(faultSegments,zoneRadius,gridSpacing, new Location(0,0));
    /**
     * The rest will have to be filled in for a "Sausage" Geographic
     * Region on a SausageGeographicRegion is defined.
     */
  }

  /**
   * get_TypeIIAftershockZone
   * This needs to be changed to return a sausage region once
   * this type of region is defined.
   */
  public GriddedRegion get_TypeIIAftershockZone() {
    return typeIIAftershockZone;
  }

  /**
   * getTypeIIFaultModel
   */
  public LocationList getTypeIIFaultModel() {
    return faultSegments;
  }

  /**
   * Bin the given lat/lon values into the given grid.  The result
   * is a grid with the number of epicenters occurring in each grid box.
   *
   * @param lats array of latitudes for each event in the catalog
   * @param lons array of latitudes for each event in the catalog
   * @param minLat minimum latitude of grid
   * @param maxLat maximum latitude of grid
   * @param minLon minimum longitude of grid
   * @param maxLon maximum longitude of grid
   * @param boxSize grid spacing
   * @return array representing the grid with each entry denoting
			  the number of epicenters occuring in the grid box
   */
  public static short[] eventMapFromCatalog(float[] lats, float[] lons,
		  float minLat, float maxLat, float minLon, float maxLon, float boxSize) {
	  
      int numberOfLatBoxes = Math.round((maxLat - minLat) / boxSize);
      int numberOfLonBoxes = Math.round((maxLon - minLon) / boxSize);
      short[] eventMap = new short[numberOfLatBoxes * numberOfLonBoxes];

      int numberOfEqks = lats.length;

      for (int i = 0;i < numberOfEqks;i++){
          int latPosition = binToWhichValueBelongs(minLat, maxLat, boxSize, lats[i], false);
          int lonPosition = binToWhichValueBelongs(minLon, maxLon, boxSize, lons[i], false);

          // check to make sure this event is in the study region
          if(latPosition > -1 && lonPosition > -1){
              int cellPosition = latPosition * numberOfLonBoxes + lonPosition;
              // add this event in the appropriate box
              eventMap[cellPosition]++;
          }
      }
      return eventMap;
  }

  /**
   * Given a binning specified by minimum value, maximum value, and bin size,
   * determine to which bin a given value belongs.
   *
   * @param gridMin minimum grid value
   * @param gridMax maximum grid value
   * @param binSize size of each bin
   * @param value value which we want to bin
   * @param allowOverflow answer to the question of whether the value should
   * be allowed to reside outside the grid.  If allowOverflow is false and
   * the value doesn't belong in the grid, -1 is returned.  Otherwise, the
   * bin number is returned, regardless of whether it falls outside the grid
   * limits.
   * @return the bin to which the value of interest belongs
   */
  public static int binToWhichValueBelongs(float gridMin, float gridMax,
          float binSize, float value, boolean allowOverflow) {
      int bigNumber = 10000;

      // Scale the minimum bin value.  To avoid rounding errors, we floor the
      // absolute value of the scaled value, then multiply this by the sign
      // of the original value
      int min_int = (int) (Math.signum(gridMin) *
              (float) Math.floor(Math.abs(gridMin * bigNumber)));
      // Scale the cell size.
      int cellSize_int = Math.round(binSize * bigNumber);
      //  Scale the value to be binned.  Avoid rounding errors as above
      int value_int = (int) (Math.signum(value) * (float) Math.floor(
              Math.abs(value * bigNumber)));
      if ((value < gridMin) || (value >= gridMax) && !allowOverflow) {
          return -1;
      }
      float position_f = (float) (value_int - min_int) / (float) cellSize_int;
      int position = (int) Math.floor(position_f);
      return position;
  }

  /**
   * Compute the Vincenty distance (in km) b/w two lat/lon points,
		each of which is specified in decimal degrees.  We use the WGS-84
		model for the Earth's ellipsoid.
   *
   * @param latOrigin first latitude point
   * @param lonOrigin first longitude point
   * @param latDestination second latitude point
   * @param lonDestination second longitude point
   * @return Vincenty distance b/w specified lat-lon points
   */
  public static float vincentyDistanceBetweenPoints(float latOrigin,
		  float lonOrigin, float latDestination, float lonDestination) {
      double a = 6378.137;
      double b = 6356.7523142;
      double f = (a - b) / a;

      // The distance formula expects lat, lon in radians, so we need to convert them from degrees
      latOrigin = (float) Math.toRadians(latOrigin);
      lonOrigin = (float) Math.toRadians(lonOrigin);
      latDestination = (float) Math.toRadians(latDestination);
      lonDestination = (float) Math.toRadians(lonDestination);

      double L = lonOrigin - lonDestination;
      double U_1 = Math.atan((1 - f) * Math.tan(latOrigin));
      double U_2 = Math.atan((1 - f) * Math.tan(latDestination));

      double lambda = L;
      double lambdaPrime = 2 * Math.PI;
      double cosSquaredAlpha = 0;
      double sinSigma = 0;
      double cosSigma = 0;
      double cos2Sigma_m = 0;
      double sigma = 0;
      double epsilon = 1e-7;
      int iterationCounter = 0;
      while (Math.abs(lambda - lambdaPrime) > epsilon) {
          double temp1 = Math.cos(U_2) * Math.sin(lambda);
          double temp2 = Math.cos(U_1) * Math.sin(U_2) - Math.sin(U_1) * Math.cos(U_2) * Math.cos(lambda);
          sinSigma = Math.sqrt(temp1 * temp1 + temp2 * temp2);
          cosSigma = Math.sin(U_1) * Math.sin(U_2) + Math.cos(U_1) * Math.cos(U_2) * Math.cos(lambda);
          sigma = Math.atan2(sinSigma, cosSigma);
          double sinAlpha = Math.cos(U_1) * Math.cos(U_2) * Math.sin(lambda) / (sinSigma + Double.MIN_VALUE);
          cosSquaredAlpha = 1 - sinAlpha * sinAlpha;
          cos2Sigma_m = Math.cos(sigma) - 2 * Math.sin(U_1) * Math.sin(U_2) / (cosSquaredAlpha + Double.MIN_VALUE);
          double C = f / 16 * cosSquaredAlpha * (4 + f * (4 - 3 * cosSquaredAlpha));
          lambdaPrime = lambda;
          lambda = L + (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2Sigma_m + C * cosSigma * (-1 + 2 * cos2Sigma_m * cos2Sigma_m)));
          iterationCounter++;
          if (iterationCounter > 100){

        	  System.err.println("GeoUtil.vincentyDistanceBetweenPoints: Convergence is a long time coming, distance may not be reliable");
              System.err.println("(lat_1, long_1) = (" + latOrigin + ", " + lonOrigin + "), (lat_2, long_2) = (" + latDestination + ", " + lonDestination + ")");
              break;
          }
      }

      double uSquared = cosSquaredAlpha * (a * a - b * b) / (b * b);
      double A = 1 + uSquared / 16384 * (4096 + uSquared * (-768 + uSquared * (320 - 175 * uSquared)));
      double B = uSquared / 1024 * (256 + uSquared * (74 - 47 * uSquared));
      double deltaSigma = B * sinSigma * (cos2Sigma_m + B / 4 * (cosSigma * (-1 + 2 * cos2Sigma_m * cos2Sigma_m - B / 6 *
              cos2Sigma_m * (-3 + 4 * sinSigma * sinSigma * (-3 + 4 * cos2Sigma_m * cos2Sigma_m)))));

      float distance = (float) (b * A * (sigma - deltaSigma));
      return (distance);
  }
  
}
