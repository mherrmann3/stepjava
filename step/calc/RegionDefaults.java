package org.opensha.step.calc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.opensha.commons.util.FileUtils;



/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2002</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class RegionDefaults {
	private static Logger logger = Logger.getLogger(RegionDefaults.class);
	
	public RegionDefaults() {
	}

	/**
	 * This class contains many of the variables that are specific
	 * to a region.  Default values are set. 
	 * 
	 */

	private static final String CONFIG_FILE = "config/defaults.properties";
	public static String INPUT_DIR = "data/mattg_test"; ///home/baishan/eclipse/workspace/opensha/trunk/data/mattg_test
	public static String OUTPUT_DIR = "output";
	
	public static  int GENERIC_MODEL_TYPE = 0; //0==r&j, 1= new
	//test model, CSEP has special format requirement
	public static final int MODEL_FORMAT_CSEP = 1;
	public static final int MODEL_FORMAT_OTHER = 0;
	public static int MODEL_FORMAT = MODEL_FORMAT_OTHER;
	public static boolean PARAM_FILE_BY_COMMAND_ARGS = false;//for csep, specify param file in commandline args?
	
	public static int REGION_CF = 0;
	public static int REGION_NZ = 1;
	
	
	//input files
	public static int externalModel; // +[MH] whether to use external model rates (i.e., ETAS)
	public static String externalModelRates;  // +[MH] file path
	public static String paramFilePath =  INPUT_DIR + "/CSEP_params.txt";//csep params
	public static String cubeFilePath =  INPUT_DIR + "/merge_NZ.nts"; //"/merge.nts", merge_synthNZ
	public static String backgroundHazardPath = INPUT_DIR +  "/STEP_NZHazProb.txt"; //STEP_NZHazProb.txt STEP_backGround
	public static String BACKGROUND_RATES_FILE_NAME =  INPUT_DIR +  "/NZdailyRates.txt"; //AllCal96ModelDaily.txt;//"org/opensha/sha/earthquake/rupForecastImpl/step/AllCal96ModelDaily.txt";
	public static String siteAmpFile =  INPUT_DIR +  "/siteAmp.dat";
	
	//output files
	public static String outputHazardPath = OUTPUT_DIR + "/STEP_Probs.txt"; 
	public static String STEP_AftershockObjectFile = OUTPUT_DIR +  "/STEP_AftershockObj";
	public static String outputAftershockRatePath =  OUTPUT_DIR + "/TimeDepRates.txt";
	//this is for Damage States
	public static String outputHazCurvePath = OUTPUT_DIR + "/HazCurve_Probs.txt";
	//STEP_Rates
	public static String outputSTEP_Rates = OUTPUT_DIR + "/STEP_Rates.txt";
	public static int outputSTEP_RatesAllBins = 0;
	//STEP_Weights
	public static String outputSTEP_Weights = OUTPUT_DIR + "/STEP_Weights.txt";
	
	public static double minMagForMainshock = 3.0;
	public static double minForecastMag = 4.0;
	public static double maxForecastMag = 8.0;
	public static double deltaForecastMag = 0.1;

	public static double forecastLengthDays = 1;
	public static boolean startForecastAtCurrentTime = true;
	public static GregorianCalendar forecastStartTime;  // set this if startForecastAtCurrentTime is False
	//main.shock.time=30/5/2009 13:25:00
	public static GregorianCalendar EVENT_START_TIME;
	//days before current time, from which periods events are subject to change
	public static int daysFromQDM_Cat = 7;

	//the minimum radius for aftertshock zone
	public static  double Min_Aftershock_R = 5;

	//California
	public final static double searchLatMin_CF = 32.0;
	public  final static double searchLatMax_CF = 42.2;
	public  final static double searchLongMin_CF = -124.6;
	public  final static double searchLongMax_CF = -112;
	//nz 
	public final static double searchLatMin_NZ = -47.95;
	public  final static double searchLatMax_NZ = -34.05;
	public  final static double searchLongMin_NZ = 164.05;
	public  final static double searchLongMax_NZ = 179.95; //-176
	

	public static double searchLatMin = searchLatMin_NZ;
	public static double searchLatMax = searchLatMax_NZ;
	public static double searchLongMin = searchLongMin_NZ;
	public static double searchLongMax = searchLongMax_NZ;	

	public static double gridSpacing = 0.1;
	public static double gridPrecisionCF = 0.1;
	public static double gridPrecisionNZ = 0.01d;
	public static double gridPrecision  = gridPrecisionNZ;
	public static double grid_anchor = 0.05; //0.05 for nz
	//min and max depth for CSEP output, z_min and z_max can be 0 and 20
	public static double MIN_Z = 0;
	public static double MAX_Z = 30;
	
//	public static double addToMc = 0.02;
	// [MH] you mean 0.2, right? done.
	public static double addToMc = .2;

	// this is for defining the fault surface for the aftershock zone.
	// 2D for now so the values are the same.
	public static double lowerSeismoDepth = 10.0;
	public static double upperSeismoDepth = 10.0;

	public static boolean useFixed_cValue = true;

	// set the parameters for the AIC Calcs for the model elements
	public static int genNumFreeParams = 0;
	public static int seqNumFreeParams = 0;
	public static int spaNumFreeParams = 3;  // should be 4 if c is not fixed

	// the minimum mag to be used when comparing the cummulative of the 
	// background to that of an individual sequence
	public static int minCompareMag = 0;

	public static final double RAKE=0.0;
	public static final double DIP=90.0;


	//the cutoff distance (of the grid spacing) in calculating forecast
	public static double CUTOFF_DISTANCE = 0.5;
	//event data source
	public static final int  EVENT_SOURCE_FILE = 0;
	public static final int  EVENT_SOURCE_GEONET = 1;
	public static final int  EVENT_SOURCE_CHINA= 2;
	public static final int  EVENT_SOURCE_CSEP= 3;
	
	public static int EVENT_DATA_SOURCE = EVENT_SOURCE_FILE; //specify the source to query quakes
	
	//forecast parameters 
	public static boolean FORECAST_PARAM_FIXED = true;
	public static double FORECAST_A_VALUE = -1.67; //-2.18
	public static double FORECAST_B_VALUE = 0.91;//0.84
	public static double FORECAST_P_VALUE = 1.08;//1.05	  
	public static double FORECAST_C_VALUE = 0.05;
	//new generic parameters
	public static double FORECAST_alpha_VALUE ; // Growth exponent
	public static double FORECAST_M1_VALUE; // Magnitude with at least one aftershock above threshold magnitude
	public static double FORECAST_Iou_VALUE; // Omori-Utsu integral
	public static double thresholdMagMA; // threshold magnitude used for mean abundance calculation
	
	  
	public static String GEONET_QUAKEML_URL="http://app-dev.geonet.org.nz/services/quake/quakeml/1.0.1/query";
	public static String DEFAULT_TEST_TIMES = "{1}{1}{1,3,7,30,100,300}";
	//default.test.read.back
	public static int DEFAULT_TEST_READ_BACK = 7;
	public static SimpleDateFormat dateformater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	public static final String PATTERN_SPACE = "[ \t]+";
	public static  boolean SAVE_MODELS = true;
	
	// +[MH] Hazard parameters
	public static double intensityLevel; // Intensity to be exceeded
	public static double rateDepth; // Intensity to be exceeded
	
    static{    	 
    	initProperties();
    	// RegionDefaults.setRegion(RegionDefaults.REGION_NZ);
		}
	/**
	 * define the search boundary
	 * used to switch regions (e.g. California, NZ)
	 * @param minLat
	 * @param maxLat
	 * @param minLon
	 * @param maxLon
	 */
	public static  synchronized void setBoundary(double minLat, double maxLat, double minLon, double maxLon){
		searchLatMin = minLat;
		searchLatMax = maxLat;
		searchLongMin = minLon;
		searchLongMax = maxLon;
	}

	/**
	 * set region default to CF or NZ
	 */
	public static  synchronized void setRegion(int regionNum){
		if(regionNum == REGION_CF){
			cubeFilePath =  INPUT_DIR + "/merge.nts"; //"/merge.nts", merge_synthNZ
			backgroundHazardPath = INPUT_DIR +  "/STEP_backGround.txt"; //STEP_NZHazProb.txt STEP_backGround
			BACKGROUND_RATES_FILE_NAME =  INPUT_DIR +  "/AllCal96ModelDaily.txt"; //AllCal96ModelDaily.txt;//"org/opensha/sha/earthquake/rupForecastImpl/step/AllCal96ModelDaily.txt";

			gridPrecision  = gridPrecisionCF;
			grid_anchor = 0; //0 for california
			setBoundary(RegionDefaults.searchLatMin_CF, RegionDefaults.searchLatMax_CF,
					RegionDefaults.searchLongMin_CF, RegionDefaults.searchLongMax_CF);

		}else if(regionNum == REGION_NZ){
			cubeFilePath =  INPUT_DIR + "/merge_NZ.nts"; //"/merge.nts", merge_synthNZ
			backgroundHazardPath = INPUT_DIR +  "/STEP_NZHazProb.txt"; //STEP_NZHazProb.txt STEP_backGround
			BACKGROUND_RATES_FILE_NAME =  INPUT_DIR +  "/NZdailyRates.txt"; //AllCal96ModelDaily.txt;//"org/opensha/sha/earthquake/rupForecastImpl/step/AllCal96ModelDaily.txt";

			gridPrecision  = gridPrecisionNZ;
			grid_anchor = 0.05; //0.05 for nz

			setBoundary(RegionDefaults.searchLatMin_NZ, RegionDefaults.searchLatMax_NZ,
					RegionDefaults.searchLongMin_NZ, RegionDefaults.searchLongMax_NZ);

		}
	}

	/**
	 * load properties from config file	
	 */
	public static  synchronized void initProperties( ){
		dateformater.setTimeZone(TimeZone.getTimeZone("UTC"));  
		Properties props = new Properties();
		//URL url = ClassLoader.getSystemResource(CONFIG_PATH);
		try {
			System.out.println("CONFIG_FILE=" + new File(CONFIG_FILE).getAbsolutePath());
			props.load(new FileInputStream(CONFIG_FILE));
			
			//1. input dir
			INPUT_DIR =  props.getProperty("data.dir", "data/mattg_test");
			// +[MH] (optional) Read in external rates
			externalModel = Integer.parseInt(props.getProperty("external.model", "0"));
			externalModelRates = INPUT_DIR + "/" +  props.getProperty("input.file.rates", "ETAS_Rates.xml");
			backgroundHazardPath = INPUT_DIR + "/" +  props.getProperty("input.file.bg.haz", "STEP_NZHazProb.txt");
			BACKGROUND_RATES_FILE_NAME = INPUT_DIR + "/" +  props.getProperty("input.file.bg.rates", "NZdailyRates.txt");
			siteAmpFile = INPUT_DIR + "/" +  props.getProperty("input.file.siteamp", "siteAmp");
			
			//2. output dir
			OUTPUT_DIR  =  props.getProperty("output.dir", "output");
			File outputDirectory = new File(OUTPUT_DIR);
			File outputParent = outputDirectory.getParentFile();
			if(outputParent != null && !outputParent.exists()){
				outputParent.mkdir();
			}
			if(!outputDirectory.exists()){
				outputDirectory.mkdir();
			}
			logger.info("outputDirectory " + outputDirectory.getAbsolutePath());
			outputHazardPath = OUTPUT_DIR + "/" +  props.getProperty("output.file.step.prob", "STEP_Probs.txt");
			STEP_AftershockObjectFile = OUTPUT_DIR + "/" +  props.getProperty("output.file.step.aftershock.obj", "STEP_AftershockObj");
			
			outputHazCurvePath = OUTPUT_DIR + "/" +  props.getProperty("output.file.haz.curv.prob", "HazCurve_Probs.txt");
			
			//3. param values
			minMagForMainshock = Double.parseDouble(props.getProperty("min.mag.main", "3.0"));
			minForecastMag = Double.parseDouble(props.getProperty("min.mag.forcast", "4.0"));;//4.0;
			maxForecastMag = Double.parseDouble(props.getProperty("max.mag.forcast", "8.0"));;//8.0;
			// [MH] For sensitivity analysis
//			maxForecastMag = maxForecastMag + 0.5;
			
			deltaForecastMag = Double.parseDouble(props.getProperty("delta.mag.forcast", "0.1"));;//0.1;						
			
			daysFromQDM_Cat = Integer.parseInt(props.getProperty("days.from.qdm", "7.0"));;//7;	 
			Min_Aftershock_R = Double.parseDouble(props.getProperty("min.aftershock.radium", "5.0"));;//5;
			gridSpacing = Double.parseDouble(props.getProperty("grid.spacing", "0.1")); ;//0.1;	 
			gridPrecision  = Double.parseDouble(props.getProperty("grid.precision", "0.01")); ;//0.01
			grid_anchor  = Double.parseDouble(props.getProperty("grid.anchor", "0.05")); ;//0.05
			CUTOFF_DISTANCE = Double.parseDouble(props.getProperty("grid.cutoff", "0.5")); ;//0.5
			
			//4. grid coords	  
			searchLatMin = Double.parseDouble(props.getProperty("bg.min.lat", "-47.95")); 
			searchLatMax = Double.parseDouble(props.getProperty("bg.max.lat", "-34.05")); 
			searchLongMin = Double.parseDouble(props.getProperty("bg.min.lon", "164.05")); 
			searchLongMax =Double.parseDouble(props.getProperty("bg.max.lon", "179.95"));
			
			//5. model format
			MODEL_FORMAT = Integer.parseInt(props.getProperty("model.format", "" + MODEL_FORMAT_OTHER)); 
			SAVE_MODELS = Integer.parseInt(props.getProperty("save.models", "1" )) == 1; 
			logger.info("SAVE_MODELS " + SAVE_MODELS);
			//5.1 genric model type
			GENERIC_MODEL_TYPE =  Integer.parseInt(props.getProperty("generic.model.type", "0"  )); 
			
			//6.model dependent params
			if(MODEL_FORMAT == MODEL_FORMAT_CSEP){
				PARAM_FILE_BY_COMMAND_ARGS = Integer.parseInt(props.getProperty("params.file.option", "0")) == 1;
				logger.info("PARAM_FILE_BY_COMMAND_ARGS " + PARAM_FILE_BY_COMMAND_ARGS);
				if(!PARAM_FILE_BY_COMMAND_ARGS){
//					paramFilePath = props.getProperty("model.params.file", INPUT_DIR + "/CSEP_params.txt");				
//					setCsepParams(paramFilePath);
					//there is no need for me (MH) to specify an additional param file,
					// when there is a config (defaults.properties) file...
					cubeFilePath = INPUT_DIR + "/" +  props.getProperty("input.file.cube", "merge_NZ.nts");
					EVENT_START_TIME  = parseTime2Cal(props.getProperty("event.start.time"));	
					forecastStartTime = parseTime2Cal(props.getProperty("forecast.start.time"));
					forecastLengthDays = Double.parseDouble(props.getProperty("forecast.len.days", "1.0"));;//1;				
					outputAftershockRatePath = OUTPUT_DIR + "/" +  props.getProperty("output.file.step.rates", "TimeDepRates.txt");
				}
				//other properties
				outputSTEP_Rates = OUTPUT_DIR + "/" +  props.getProperty("output.file.step.rates", "STEP_Rates.txt");
				outputSTEP_Weights = OUTPUT_DIR + "/" +  props.getProperty("output.file.step.weights", "STEP_Weigths.txt");
				startForecastAtCurrentTime = false;
				EVENT_DATA_SOURCE = EVENT_SOURCE_CSEP;				
			}else{//not csep
				cubeFilePath = INPUT_DIR + "/" +  props.getProperty("input.file.cube", "merge_NZ.nts");
				EVENT_START_TIME  = parseTime2Cal(props.getProperty("event.start.time"));	
				forecastStartTime = parseTime2Cal(props.getProperty("forecast.start.time"));
				startForecastAtCurrentTime = Integer.parseInt(props.getProperty("start.forecast.current", "1")) == 1 ;
				forecastLengthDays = Double.parseDouble(props.getProperty("forecast.len.days", "1.0"));;//1;				
				outputSTEP_Rates = OUTPUT_DIR + "/" +  props.getProperty("output.file.step.rates", "STEP_Rates.txt");
				outputSTEP_Weights = OUTPUT_DIR + "/" +  props.getProperty("output.file.step.weights", "STEP_Weigths.txt");
				outputSTEP_RatesAllBins = Integer.parseInt(props.getProperty("output.file.step.rates.bins", "0"));
				outputAftershockRatePath = OUTPUT_DIR + "/" +  props.getProperty("output.file.time.dep.rates", "TimeDepRates.txt");
				EVENT_DATA_SOURCE = Integer.parseInt(props.getProperty("quake.datasource", "0"));
				 //. geonet quake url
				//quake.datasource=0			
				GEONET_QUAKEML_URL = props.getProperty("geonet.quake.url", "http://app-dev.geonet.org.nz/services/quake/quakeml/1.0.1/query");
			}	
		
			//6. default test times ##default continuous test times{minutes}{hours}{days}
		     //default.test.times={1}{1}{1,3,7,30,100,300}
			DEFAULT_TEST_TIMES = props.getProperty("default.test.times");
			//default.test.read.back
			DEFAULT_TEST_READ_BACK = Integer.parseInt(props.getProperty("default.test.read.back", "7"));
			//7. forecast parameters
			//forecast.param.fixed=0
			FORECAST_PARAM_FIXED = 1==Integer.parseInt(props.getProperty("forecast.param.fixed", "0"));
			FORECAST_A_VALUE = Double.parseDouble(props.getProperty("a.value", "-1.67"));//-1.67; //-2.18
			FORECAST_B_VALUE = Double.parseDouble(props.getProperty("b.value", "0.91"));//0.91;//0.84
			FORECAST_P_VALUE = Double.parseDouble(props.getProperty("p.value", "1.08"));//1.08;//1.05	  
			FORECAST_C_VALUE = Double.parseDouble(props.getProperty("c.value", "0.05"));//0.05;
			// +[MH] New generic parameters
			FORECAST_alpha_VALUE = Double.parseDouble(props.getProperty("alpha.value", "1"));
			FORECAST_M1_VALUE = Double.parseDouble(props.getProperty("M1.value", "5.4"));
			FORECAST_Iou_VALUE = Double.parseDouble(props.getProperty("Iou.value", "5.4"));
			thresholdMagMA = Double.parseDouble(props.getProperty("Mth.value", "4.0"));
			
			// +[MH] Hazard params
			intensityLevel = Double.parseDouble(props.getProperty("intensity.level", "6.0"));
			rateDepth = Double.parseDouble(props.getProperty("rate.depth", "0.0"));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void setCsepParams(String filePath) throws FileNotFoundException, IOException {	
		logger.info(">> setCsepParams filePath " + filePath);
		//load csel params
		ArrayList<String> fileLines = FileUtils.loadFile(filePath);
		//logger.error(e);
		String line = fileLines.get(0);
		logger.info("catalog start date " + line);
		EVENT_START_TIME  = parseCsepTime2Cal(line);
		logger.info("EVENT_START_TIME " +  dateformater.format(EVENT_START_TIME.getTime()));
		line = fileLines.get(1);
		logger.info("catalog end date " + line);
		forecastStartTime =  parseCsepTime2Cal(line);
		logger.info("forecastStartTime " +  dateformater.format(forecastStartTime.getTime()));
		line = fileLines.get(2);
		logger.info("forecast length " + line);
		forecastLengthDays = Integer.parseInt(line.trim());
		line = fileLines.get(3);
		logger.info("catalog input file " + line);
		cubeFilePath = line;
		line = fileLines.get(4);
		logger.info("forecast output file " + line);
		outputAftershockRatePath = line;//OUTPUT_DIR + "/" +  props.getProperty("output.file.time.dep.rates", "TimeDepRates.txt");	

		line = fileLines.get(5);
		logger.info("background rate input file " + line);
		BACKGROUND_RATES_FILE_NAME = line;

		
		
	}

	private static GregorianCalendar parseCsepTime2Cal(String timestr) {
		GregorianCalendar cal = null;
		if(timestr != null){
			String[] dateElements = timestr.split(PATTERN_SPACE);
			//logger.info("dateElements "+ dateElements.length );
			if(dateElements.length == 6){				
				cal = STEP_main.getCurrentGregorianTime();
				cal.set(GregorianCalendar.DAY_OF_MONTH, Integer.parseInt(dateElements[0]));
				cal.set(GregorianCalendar.MONTH, Integer.parseInt(dateElements[1]) - 1);
				cal.set(GregorianCalendar.YEAR, Integer.parseInt(dateElements[2]));
				cal.set(GregorianCalendar.HOUR_OF_DAY, Integer.parseInt(dateElements[3]));
				cal.set(GregorianCalendar.MINUTE, Integer.parseInt(dateElements[4]));
				cal.set(GregorianCalendar.SECOND, Integer.parseInt(dateElements[5]));
			}	
		}	
		return cal;
	}

	private static GregorianCalendar parseTime2Cal(String timestr  ) {
		GregorianCalendar cal ;
		if(timestr != null){
			try {
				Date time = dateformater.parse(timestr);
				cal = STEP_main.getCurrentGregorianTime();
				cal.setTime(time);
			} catch (ParseException e) {						
				logger.error(e);
				cal = null;
			}
		}else{
			cal = null;
		}	
		return cal;
	}


}
