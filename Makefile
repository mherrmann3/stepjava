# 
# Top level Makefile to build STEP_Java/opensha/src/org executables

.PHONY: default all install clean testclean

TARGET ?= all

# ----------------------------------------------------------------------

SUBDIRS = step

###### All Rules 
all:
	@for dir in $(SUBDIRS); do \
		pushd $$dir; $(MAKE) all; popd; \
	done
	
###### Install Rules
install:
	@for dir in $(SUBDIRS); do \
		pushd $$dir; $(MAKE) install; popd; \
	done


###### Test Rules
check:
	@for dir in $(SUBDIRS); do \
		pushd $$dir; $(MAKE) check; popd; \
	done

####### Clean Rules
clean:
	@for dir in $(SUBDIRS); do \
		cd $$dir; $(MAKE) clean; cd $(PWD); \
	done

####### Test clean rules
testclean:
	@for dir in $(SUBDIRS); do \
		cd $$dir; $(MAKE) testclean; cd $(PWD); \
	done
