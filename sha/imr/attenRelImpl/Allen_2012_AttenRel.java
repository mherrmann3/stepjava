package org.opensha.sha.imr.attenRelImpl;

import org.opensha.commons.data.NamedObjectAPI;
import org.opensha.commons.data.Site;
import org.opensha.commons.exceptions.InvalidRangeException;
import org.opensha.commons.exceptions.ParameterException;
import org.opensha.commons.param.DoubleConstraint;
//import org.opensha.commons.param.DoubleParameter;
import org.opensha.commons.param.StringConstraint;
import org.opensha.commons.param.event.ParameterChangeEvent;
import org.opensha.commons.param.event.ParameterChangeListener;
import org.opensha.commons.param.event.ParameterChangeWarningListener;
import org.opensha.sha.earthquake.EqkRupture;
import org.opensha.sha.imr.AttenuationRelationship;
import org.opensha.sha.imr.ScalarIntensityMeasureRelationshipAPI;
import org.opensha.sha.imr.param.EqkRuptureParams.MagParam;
import org.opensha.sha.imr.param.IntensityMeasureParams.MMI_Param;
import org.opensha.sha.imr.param.OtherParams.StdDevTypeParam;
import org.opensha.sha.imr.param.PropagationEffectParams.DistanceHypoParameter;
//import org.opensha.sha.imr.param.PropagationEffectParams.DistanceRupParameter;
//import org.opensha.sha.imr.param.PropagationEffectParams.DistanceJBParameter;
import org.opensha.sha.imr.param.SiteParams.IAmp_Param;

/**
 * <b>Title:</b> Allen_2012_AttenRel
 * <p>
 * 
 * <b>Description:</b>
 * This implements the Allen 2012 "Intensity attenuation for active crustal regions",
 * using the second approach: hypocentral distance Rhyp (not Rrup).
 *  
 * 
 * Supported Intensity-Measure Parameters:
 * <p>
 * <UL>
 * <LI>mmiParam - Modified Mercalli Intensity (representing EMS)
 * </UL>
 * <p>
 * Other Independent Parameters:
 * <p>
 * <UL>
 * <LI>magParam - moment Magnitude
 * <LI>distanceEpiParam - epicentral distance
 * <LI>stdDevTypeParam - The type of standard deviation
 * <LI>IAmpParam - The site amplification (in intensity units)
 * </UL>
 * </p>
 * 
 * 
 * 
 * @author Marcus Herrmann
 * @created created April, 2013
 * @version 0.1
 */
public class Allen_2012_AttenRel extends AttenuationRelationship implements
		ScalarIntensityMeasureRelationshipAPI, NamedObjectAPI,
		ParameterChangeListener {
	private static double standardDeviation = 1.0d;
	private static final Double DISTANCE_EPI_WARN_MIN = 0.0;
	private static final Double DISTANCE_EPI_WARN_MAX = 200.0;
	private static final Double MAG_WARN_MIN = 3.2;
	private static final Double MAG_WARN_MAX = 6.9;

	// By now the calculator does not observe parameter changes.
	// So this is a UI listener that is only introduced for consistency.
	private ParameterChangeWarningListener warningListener = null;
	// hypocentral distance - never read just defined to comply with the
	double rHypo;
	// Rupture magnitude - never read just defined to comply with the
	private double mag;
	// Standard Dev type - never read just defined to comply with the
	private String stdDevType;
	// See above: unused UI stuff
	private boolean parameterChange;
	// Intensity Amplification Factor
	private double IAmp;
	// The parameters
	double c0 = 2.085;	double c1 = 1.428;	double c2 = -1.402;	double c4 = 0.078;
	double m1 = -0.209;	double m2 = 2.042;
	double s1 = 0.82;	double s2 = 0.37;	double s3 = 22.9;
	

	public Allen_2012_AttenRel(ParameterChangeWarningListener wl) {
		super();
		this.warningListener = wl;
		initSupportedIntensityMeasureParams();
		initEqkRuptureParams();
		initPropagationEffectParams();
		initSiteParams();
		initOtherParams();
		initParameterEventListeners();
	} // constructor

	@Override
	protected void initSupportedIntensityMeasureParams() {
		// The MMI parameter
		mmiParam = new MMI_Param();
		mmiParam.setNonEditable();
		mmiParam.addParameterChangeWarningListener(warningListener);
		/*
		 * "supportedIMParams" is defined and initialised in class
		 * IntensityMeasureRelationship. -> It is save to use it here.
		 * Suggestion for an OpenSha refactoring: TODO: Avoid uncontrolled
		 * access by the subclasses. Set these members private in the super
		 * class and provide access via getters and setters and let the lazy
		 * init!.
		 */
		// getSupportedIntensityMeasuresList().clear();
		supportedIMParams.clear();
		// getSupportedIntensityMeasuresList().addParameter(mmiParam);
		supportedIMParams.addParameter(mmiParam);
	}

	@Override
	protected void initEqkRuptureParams() {
		magParam = new MagParam(MAG_WARN_MIN, MAG_WARN_MAX);
		/*
		 * eqkRuptureParams is defined and initialised in class
		 * IntensityMeasureRelationship. -> It is save to use it here.
		 * Suggestion for an OpenSha refactoring: TODO: Avoid uncontrolled
		 * access by the subclasses. Set these members private in the super
		 * class and provide access via getters and setters and let the lazy
		 * init!.
		 */
		// getEqkRuptureParamsList().clear();
		eqkRuptureParams.clear();
		// getEqkRuptureParamsList().addParameter(magParam);
		eqkRuptureParams.addParameter(magParam);
	}

	@Override
	protected void initPropagationEffectParams() {
		distanceHypoParameter = new DistanceHypoParameter(0.0);
		distanceHypoParameter
				.addParameterChangeWarningListener(warningListener);
		DoubleConstraint warn = new DoubleConstraint(DISTANCE_EPI_WARN_MIN,
				DISTANCE_EPI_WARN_MAX);
		warn.setNonEditable();
		distanceHypoParameter.setWarningConstraint(warn);
		distanceHypoParameter.setNonEditable();
		/*
		 * "propagationEffectParams" is defined and initialised in class
		 * IntensityMeasureRelationship. -> It is save to use it here.
		 * Suggestion for an OpenSha refactoring: TODO: Avoid uncontrolled
		 * access by the subclasses. Set these members private in the super
		 * class and provide access via getters and setters and let the lazy
		 * init!.
		 */
		// getPropagationEffectParamsList().addParameter(distanceEpiParameter);
		propagationEffectParams.addParameter(distanceHypoParameter);
	} // initPropagationEffectParams()

	@Override
	protected void initSiteParams() {
		IAmpParam = new IAmp_Param();

		// getSiteParamsList().clear();
		siteParams.clear();
		siteParams.addParameter(IAmpParam);
	}

	@Override
	protected void initOtherParams() {
		super.initOtherParams();
		stdDevTypeParam = createStdDevTypeParam();
		// StringConstraint stdDevTypeConstraint = new StringConstraint();
		// stdDevTypeConstraint.addString(StdDevTypeParam.STD_DEV_TYPE_NONE);
		// stdDevTypeConstraint.setNonEditable();
		// stdDevTypeParam = new StdDevTypeParam(stdDevTypeConstraint);
		otherParams.addParameter(stdDevTypeParam);
	}

	private StdDevTypeParam createStdDevTypeParam() {
		StringConstraint stdDevTypeConstraint = new StringConstraint();
		stdDevTypeConstraint.addString(StdDevTypeParam.STD_DEV_TYPE_NONE);
		stdDevTypeConstraint.setNonEditable();
		/*
		 * Note: This results in an error, becaus the constructor sets different
		 * default value: StdDevTypeParam stdDev = new
		 * StdDevTypeParam(stdDevTypeConstraint);
		 */
		return new StdDevTypeParam(stdDevTypeConstraint,
				StdDevTypeParam.STD_DEV_TYPE_NONE);
	}

	@Override
	protected void setPropagationEffectParams() {
		if ((this.site != null) && (this.eqkRupture != null)) {
			distanceHypoParameter.setValue(eqkRupture, site);
		}
	}

	@Override
	public double getMean() {
		double hypocentralDistance = (Double) distanceHypoParameter.getValue();
		return getMean(magParam.getValue(), hypocentralDistance,
				IAmpParam.getValue());
	} // getMean()

	/**
	 * Sets the eqkRupture related parameters (magParam) based on the passed
	 * eqkRupture. The internally held eqkRupture object is set to the passed
	 * parameter. Warning constraints are not ignored.
	 * 
	 * @param eqkRupture
	 * 
	 */
	@Override
	public void setEqkRupture(EqkRupture eqkRupture)
			throws InvalidRangeException {

		// magParam.setValue(new Double(eqkRupture.getMag()));
		magParam.setValueIgnoreWarning(new Double(eqkRupture.getMag()));
		// setFaultTypeFromRake(eqkRupture.getAveRake());
		this.eqkRupture = eqkRupture;
		setPropagationEffectParams();

	}

	/**
	 * Sets the internally held Site object to the passed site parameter.
	 * 
	 * @param site
	 * 
	 */
	@Override
	public void setSite(Site site) throws ParameterException {
		IAmpParam.setValueIgnoreWarning((Double) site.getParameter(
				IAmp_Param.NAME).getValue());
		this.site = site;
		setPropagationEffectParams();
	}

	@Override
	public String getShortName() {
		return null;
	}

	@Override
	public void setParamDefaults() {
		magParam.setValueAsDefault();
		distanceHypoParameter.setValueAsDefault();
		mmiParam.setValueAsDefault();
		IAmpParam.setValueAsDefault();
		/*
		 * Lazy init. This method (setParamDefaults()) is public. The init
		 * method for stdDevTypeParam is called in the constructor though...
		 */
		getStdDevTypeParam().setValueAsDefault();
	}

	/**
	 * Method to be implemented of Listeners of ParameterChangeEvents.
	 */
	@Override
	public void parameterChange(ParameterChangeEvent event) {
		String pName = event.getParameterName();
		Object val = event.getNewValue();
		parameterChange = true;
		if (pName.equals(DistanceHypoParameter.NAME)) {
			rHypo = ((Double) val).doubleValue();
		} else if (pName.equals(MagParam.NAME)) {
			mag = ((Double) val).doubleValue();
		} else if (pName.equals(StdDevTypeParam.NAME)) {
			stdDevType = (String) val;
		} else if (pName.equals(IAmp_Param.NAME)) {
			IAmp = ((Double) val).doubleValue();
		}
	}

	/**
	 * Allen 2012 Attenuation Relations:
	 * 
	 * @param magnitude
	 * @param epicentralDistance
	 * @return mean MMI predicted by the equation
	 *         ("Intensity mercalli_modified_intensity")
	 */
	public double getMean(double M, double R, double IAmp) {
		double I_Obs;
		
		double Rm = m1 + m2 * Math.exp(M-5);
		
		if (R <= 50) {
			I_Obs = c0 + c1 * M + c2 * Math.log(Math.sqrt(R*R+Rm*Rm));
		} else {
			I_Obs = c0 + c1 * M + c2 * Math.log(Math.sqrt(R*R+Rm*Rm)) + c4 * Math.log(R/50);
		}
		
		// Calculate distant-dependent standardDeviation (sigma)
		standardDeviation = s1 + s2 / (1 + (R/s3)*(R/s3));
		// For sensitivity analysis
//		standardDeviation = standardDeviation * 1.2;
//		IAmp = IAmp * 1.2;

		return I_Obs + IAmp;
	} // getMean()

	/**
	 * @return The standard deviation value
	 */
	@Override
	public double getStdDev() {
		return standardDeviation;
	} // getStdDev()

	private StdDevTypeParam getStdDevTypeParam() {
		if (stdDevTypeParam == null) {
			stdDevTypeParam = createStdDevTypeParam();
		}
		return stdDevTypeParam;
	}

} // class ECOS_2002_AttenRel()
