package org.opensha.sha.imr.attenRelImpl;

import org.opensha.commons.data.NamedObjectAPI;
import org.opensha.commons.data.Site;
import org.opensha.commons.exceptions.InvalidRangeException;
import org.opensha.commons.exceptions.ParameterException;
import org.opensha.commons.param.DoubleConstraint;
//import org.opensha.commons.param.DoubleParameter;
import org.opensha.commons.param.StringConstraint;
import org.opensha.commons.param.event.ParameterChangeEvent;
import org.opensha.commons.param.event.ParameterChangeListener;
import org.opensha.commons.param.event.ParameterChangeWarningListener;
import org.opensha.sha.earthquake.EqkRupture;
import org.opensha.sha.imr.AttenuationRelationship;
import org.opensha.sha.imr.ScalarIntensityMeasureRelationshipAPI;
import org.opensha.sha.imr.param.EqkRuptureParams.MagParam;
import org.opensha.sha.imr.param.IntensityMeasureParams.MMI_Param;
import org.opensha.sha.imr.param.OtherParams.StdDevTypeParam;
import org.opensha.sha.imr.param.PropagationEffectParams.DistanceEpicentralParameter;
//import org.opensha.sha.imr.param.PropagationEffectParams.DistanceRupParameter;
//import org.opensha.sha.imr.param.PropagationEffectParams.DistanceJBParameter;
import org.opensha.sha.imr.param.SiteParams.IAmp_Param;


/**
 * <b>Title:</b> ECOS_2002_AttenRel
 * <p>
 * 
 * <b>Description:</b> This implements the REVISED ECOS-02 attenuation
 * relationship of Alvarez-Rubio (2011), originally published by the
 * Earthquake Catalog of Switzerland 2002. It has been empirically derived
 * by correlating the intensity field with the size and distance of an event
 * using bi-linear regression for either shallow and deep events. 
 * The equation returns the mean EMS intensity ("European Macroseismic
 * Scale intensity") predicted by the equation, thus representing an IPE.
 * 
 * The magnitudes of calibration have changed after 2002; therefore 
 * the revised model is used. The Formulas are:
 * ## 0-55 km distance ##
 *    Iems = 1.5248 * Mw - 0.043 * D - 0.9079 (shallow)
 *    --- Mw > 5.5 ---
 *    Iems = 1.7196 * Mw - 0.030 * D - 2.8941 (deep)
 * ## 55-200 km distance ##
 *    Iems = 1.5248 * Mw - 0.043 * D - 0.9079 (shallow)
 *    --- Mw > 5.5 ---
 *    Iems = 1.7196 * Mw - 0.030 * D - 2.8941 (deep)
 * 
 * Note: It can not discriminate between Foreland and Alpine Region
 * and the depth of an earthquake, thus fixing it to *SHALLOW FORELAND*!
 * 
 * AGAIN: F O R E L A N D!
 * 
 * The standard deviation is fixed to 0.5.
 * 
 * Supported Intensity-Measure Parameters:
 * <p>
 * <UL>
 * <LI>mmiParam - Modified Mercalli Intensity (representing EMS)
 * </UL>
 * <p>
 * Other Independent Parameters:
 * <p>
 * <UL>
 * <LI>magParam - moment Magnitude
 * <LI>distanceEpiParam - epicentral distance
 * <LI>stdDevTypeParam - The type of standard deviation
 * <LI>IAmpParam - The site amplification (in intensity units)
 * </UL>
 * </p>
 * 
 * 
 * 
 * @author Marcus Herrmann
 * @created created June, 2012, last modified April 2013
 * @version 0.4
 */
public class ECOS_2002rev_AttenRel extends AttenuationRelationship implements
        ScalarIntensityMeasureRelationshipAPI, NamedObjectAPI,
        ParameterChangeListener {
    private final double standardDeviation = 0.5d;
    private static final Double DISTANCE_EPI_WARN_MIN = 0.0;
    private static final Double DISTANCE_EPI_WARN_MAX = 200.0;
    private static final Double MAG_WARN_MIN = 3.2;
    private static final Double MAG_WARN_MAX = 6.9;

    // By now the calculator does not observe parameter changes.
    // So this is a UI listener that is only introduced for consistency.
    private ParameterChangeWarningListener warningListener = null;
    // epicentral distance - never read just defined to comply with the
    // "analogous" implemented classes ("parameterChange()"). -> refactor!
    double rEpi;
    // Rupture magnitude - never read just defined to comply with the
    // "analogous" implemented classes ("parameterChange()"). -> refactor!
    private double mag;
    // Standard Dev type - never read just defined to comply with the
    // "analogous" implemented classes ("parameterChange()"). -> refactor!
    private String stdDevType;
    // See above: unused UI stuff
    private boolean parameterChange;
	// Intensity Amplification Factor
	private double IAmp;
	// the parameters
	// shallow             deep
	private double a = -0.9079;    private double ad = -2.8941;
	private double b = 1.5248;     private double bd = 1.7196;
	private double c = -0.043;     private double cd = -0.030;
	// >= 55km
	private double a55 = -2.6539;  private double a55d = -3.9241;
	private double b55 = 1.5248;   private double b55d = 1.7196;
	private double c55 = -0.0115;  private double c55d = -0.0115;
	// Shallow-deep "boundary"
	private double zSD = 12; // made up by me [MH], not quite sure. It's probably at 8km
	
    

    public ECOS_2002rev_AttenRel(ParameterChangeWarningListener wl) {
        super();
        this.warningListener = wl;
        initSupportedIntensityMeasureParams();
        initEqkRuptureParams();
        initPropagationEffectParams();
        initSiteParams();
        initOtherParams();
        initParameterEventListeners();
    } // constructor

    @Override
    protected void initSupportedIntensityMeasureParams() {
        // The MMI parameter
        mmiParam = new MMI_Param();
        mmiParam.setNonEditable();
        mmiParam.addParameterChangeWarningListener(warningListener);
        /*
         * "supportedIMParams" is defined and initialised in class
         * IntensityMeasureRelationship. -> It is save to use it here.
         * Suggestion for an OpenSha refactoring: TODO: Avoid uncontrolled
         * access by the subclasses. Set these members private in the super
         * class and provide access via getters and setters and let the lazy
         * init!.
         */
//        getSupportedIntensityMeasuresList().clear();
         supportedIMParams.clear();
//        getSupportedIntensityMeasuresList().addParameter(mmiParam);
         supportedIMParams.addParameter(mmiParam);
    }

    @Override
    protected void initEqkRuptureParams() {
        magParam = new MagParam(MAG_WARN_MIN, MAG_WARN_MAX);
        /*
         * eqkRuptureParams is defined and initialised in class
         * IntensityMeasureRelationship. -> It is save to use it here.
         * Suggestion for an OpenSha refactoring: TODO: Avoid uncontrolled
         * access by the subclasses. Set these members private in the super
         * class and provide access via getters and setters and let the lazy
         * init!.
         */
//        getEqkRuptureParamsList().clear();
         eqkRuptureParams.clear();
//        getEqkRuptureParamsList().addParameter(magParam);
         eqkRuptureParams.addParameter(magParam);
    }

    @Override
    protected void initPropagationEffectParams() {
    	distanceEpiParameter = new DistanceEpicentralParameter(0.0);
    	distanceEpiParameter
                .addParameterChangeWarningListener(warningListener);
        DoubleConstraint warn =
                new DoubleConstraint(DISTANCE_EPI_WARN_MIN,
                        DISTANCE_EPI_WARN_MAX);
        warn.setNonEditable();
        distanceEpiParameter.setWarningConstraint(warn);
        distanceEpiParameter.setNonEditable();
        /*
         * "propagationEffectParams" is defined and initialised in class
         * IntensityMeasureRelationship. -> It is save to use it here.
         * Suggestion for an OpenSha refactoring: TODO: Avoid uncontrolled
         * access by the subclasses. Set these members private in the super
         * class and provide access via getters and setters and let the lazy
         * init!.
         */
//        getPropagationEffectParamsList().addParameter(distanceEpiParameter);
        propagationEffectParams.addParameter(distanceEpiParameter);
    } // initPropagationEffectParams()

    @Override
    protected void initSiteParams() {
    	 IAmpParam = new IAmp_Param();
    	 
//        getSiteParamsList().clear();
         siteParams.clear();
         siteParams.addParameter(IAmpParam);
    }

    @Override
    protected void initOtherParams() {
        super.initOtherParams();
        stdDevTypeParam = createStdDevTypeParam();
        // StringConstraint stdDevTypeConstraint = new StringConstraint();
        // stdDevTypeConstraint.addString(StdDevTypeParam.STD_DEV_TYPE_NONE);
        // stdDevTypeConstraint.setNonEditable();
        // stdDevTypeParam = new StdDevTypeParam(stdDevTypeConstraint);
        otherParams.addParameter(stdDevTypeParam);
    }

    private StdDevTypeParam createStdDevTypeParam() {
        StringConstraint stdDevTypeConstraint = new StringConstraint();
        stdDevTypeConstraint.addString(StdDevTypeParam.STD_DEV_TYPE_NONE);
        stdDevTypeConstraint.setNonEditable();
        /*
         * Note: This results in an error, becaus the constructor sets different
         * default value: StdDevTypeParam stdDev = new
         * StdDevTypeParam(stdDevTypeConstraint);
         */
        return new StdDevTypeParam(stdDevTypeConstraint,
                StdDevTypeParam.STD_DEV_TYPE_NONE);
    }

    @Override
    protected void setPropagationEffectParams() {
        if ((this.site != null) && (this.eqkRupture != null)) {
            distanceEpiParameter.setValue(eqkRupture, site);
        }
    }

    @Override
    public double getMean() {
        double epicentralDistance =
                (Double) distanceEpiParameter.getValue();
        return getMean(magParam.getValue(), epicentralDistance, IAmpParam.getValue());
    } // getMean()

    /**
     * Sets the eqkRupture related parameters (magParam) based on the passed
     * eqkRupture. The internally held eqkRupture object is set to the passed
     * parameter. Warning constraints are not ignored.
     * 
     * @param eqkRupture
     * 
     */
    @Override
    public void setEqkRupture(EqkRupture eqkRupture) throws InvalidRangeException {
    	
//        magParam.setValue(new Double(eqkRupture.getMag()));
        magParam.setValueIgnoreWarning(new Double(eqkRupture.getMag()));
//        setFaultTypeFromRake(eqkRupture.getAveRake());
        this.eqkRupture = eqkRupture;
        setPropagationEffectParams();
        
    }

    /**
     * Sets the internally held Site object to the passed site parameter.
     * 
     * @param site
     * 
     */
    @Override
    public void setSite(Site site) throws ParameterException {
		IAmpParam.setValueIgnoreWarning((Double) site.getParameter(
				IAmp_Param.NAME).getValue());
        this.site = site;
        setPropagationEffectParams();
    }

    @Override
    public String getShortName() {
        return null;
    }

    @Override
    public void setParamDefaults() {
        magParam.setValueAsDefault();
        distanceEpiParameter.setValueAsDefault();
        mmiParam.setValueAsDefault();
        IAmpParam.setValueAsDefault();
        /*
         * Lazy init. This method (setParamDefaults()) is public. The init
         * method for stdDevTypeParam is called in the constructor though...
         */
        getStdDevTypeParam().setValueAsDefault();
    }

    /**
     * Method to be implemented of Listeners of ParameterChangeEvents.
     */
    @Override
    public void parameterChange(ParameterChangeEvent event) {
        String pName = event.getParameterName();
        Object val = event.getNewValue();
        parameterChange = true;
        if (pName.equals(DistanceEpicentralParameter.NAME)) {
            rEpi = ((Double) val).doubleValue();
        } else if (pName.equals(MagParam.NAME)) {
            mag = ((Double) val).doubleValue();
        } else if (pName.equals(StdDevTypeParam.NAME)) {
            stdDevType = (String) val;
		} else if (pName.equals(IAmp_Param.NAME)) {
			IAmp = ((Double) val).doubleValue();
        }
    }

    /**
     * Name of equation: ECOS (2002) Attenuation Relations:
     * Immi = a + b magnitude - c epiDistance
     * 
     * @param magnitude
     * @param epicentralDistance
     * @return mean MMI predicted by the equation
     *         ("Intensity mercalli_modified_intensity")
     */
    public double getMean(double M, double epiDistance, double IAmp) {
        double I_Obs;
    	
        double h = eqkRupture.getHypocenterLocation().getDepth(); // gets the predefined depth of rates
        
        // only valid for soil class B (well consolidated sediments [EC8])
        if (epiDistance <= 55){
        	if ((M <= 5.5) && (h <= zSD))  {
        		I_Obs = a + b*M + c*epiDistance; // Shallow
        	} else {
        		I_Obs = ad + bd*M + cd*epiDistance; // Deep
        	}
        } else {
        	if ((M <= 5.5) && (h <= zSD)) {
        		I_Obs = a55 + b55*M + c55*epiDistance; // Shallow
        	} else {
        		I_Obs = a55d + b55d*M + c55d*epiDistance; // Deep
        	}
        }

//        return Math.log(I_Obs);
        return I_Obs + IAmp;
    } // getMean()

    /**
     * @return The standard deviation value
     */
    @Override
    public double getStdDev() {
        return standardDeviation;
    } // getStdDev()

    private StdDevTypeParam getStdDevTypeParam() {
        if (stdDevTypeParam == null) {
            stdDevTypeParam = createStdDevTypeParam();
        }
        return stdDevTypeParam;
    }

} // class ECOS_2002_AttenRel()
